package E002;

import java.util.Scanner;

public class Exercicio2 {
    
    public static void main(String[] args) {
       Scanner entrada = new Scanner(System.in);
       float altura,peso = 0;
       String sexo;
       
       System.out.println("Digite sua Altura ex: 1,66");
       String rep = entrada.next();
       altura = Float.parseFloat(rep.replace(",", "."));
       
       System.out.println("Digite o Seu Sexo (M para Masculino e F para Feminino)");
       sexo = entrada.next();
       
       switch(sexo.toUpperCase()){
           case "M":
               peso += ((72.7*altura)-58);
               System.out.printf("Peso Ideal Para homens:  %.2f %n",peso);
               break;
           case "F":
               peso += ((62.1*altura)-44.7);
               System.out.printf("Peso Ideal Para mulheres:  %.2f %n",peso);
               break;
           default:
               System.out.println("Opção Invalida");
               break;
       }
    }
    
}
