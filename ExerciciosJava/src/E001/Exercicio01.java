package E001;

import java.util.Scanner;

public class Exercicio01 {
    
    public static void main(String[] args) {
        Scanner entrada;
        int lado1;
        int lado2;
        int perimetro;
        int area;
        
        entrada = new Scanner(System.in);
        System.out.println("Informe o maior lado do quadrado");
        lado1 = entrada.nextInt();
        
        System.out.println("informe o menor lado do quadrado");
        lado2 = entrada.nextInt();
        
        area = lado1 * lado2;
        perimetro =2*lado1 + 2*lado2;
        
        System.out.println("A area do quadrado e " + area );
        System.out.println("O perimetro do quadrao e " + perimetro);

    }
    
}
